 var api ={
    ProducerGroups: "http://ggrids.com/ecotrust/wp-json/wp/v2/producer-groups",
    Producers: "http://ggrids.com/ecotrust/api/outbound?method=get_producers&token=528b551ae9597e2486fe87676684d3d2&account=2",
    Buyers: "http://ggrids.com/ecotrust/api/outbound?method=get_buyers&token=528b551ae9597e2486fe87676684d3d2&account=2",
    BuyerSalesAgreement: "http://ggrids.com/ecotrust/api/outbound?method=get_agreements&token=528b551ae9597e2486fe87676684d3d2&account=2",
    Technician: "http://ggrids.com/ecotrust/wp-json/wp/v2/pvtechnicians",
    PlanVivoInfo: "http://ggrids.com/ecotrust/api/outbound?method=get_pvs&token=528b551ae9597e2486fe87676684d3d2&account=2",


//method to list all  producers
showProducers: function ()
      {
     // alert('len');
           $.get(Producers,  function(data)
            {

                $('#producer_list').html('');
                var len = data.producers.length, i;

                for(i = 0; i < len; i++)
                {

                    var $pElem, $liElem, $h2Elem, $aElem;

                    $liElem = $('<li>');
                    $h2Elem = $('<h2>');
                    $aElem = $('<a>');
                    $pElem = $('<p>');

                    $h2Elem.text(data.producers[i].name);
                    $aElem.attr('id',data.producers[i].id)
                        .attr('name', data.producers[i].name)
                        .attr('class', 'ui-btn')
                        .attr('data-transition', 'none')
                        .attr('href', '#view-producer');

                    $pElem.text(data.producers[i].group);

                    $aElem .append($h2Elem)
                        .append($pElem);

                    $aElem.bind('tap', function(){
                       //alert($(this).attr('id'));
                       ViewProducer( $(this).attr('id'), $(this).attr('name'));
                    });

                    $liElem
                    .append($aElem);

                    $('#producer_list') .append($liElem);

                }
                $('#producer_list').listview('refresh');


            },'json');
        } ,

//method to view a producer
ViewProducer: function( producer_id ,name){

        $('#producer-name').text('');
        $('#producer-title').text(name);
        $('#village').text('');
        $('#country').text('');
        $('#producer-group').text('');
        $('#community').text('');
        $('#province').text('');

        $.get(Producers+'&single='+producer_id, function(data)
        {
            $('#producer-name').text(data.producers[0].name);
            $('#village').text(data.producers[0].village);
            $('#country').text(data.producers[0].country);
            $('#producer-group').text(data.producers[0].group);
            $('#community').text(data.producers[0].community);
            $('#province').text(data.producers[0].province);

        },'json');
        $('#edit-producer').bind('tap', function()
        {
           EditProducer(producer_id,name);
        });
},

//method to list all  producer groups
showProducerGroups: function ()
    {

       $.get(this.ProducerGroups,  function(data)
        {
            $('#producer_groups').html('');
            var len = data.length, i;

            for(i = 0; i < len; i++)
            {
                var $pElem, $liElem, $h2Elem, $aElem;

                $liElem = $('<li>');
                $h2Elem = $('<h2>');
                $aElem = $('<a>');
                $pElem = $('<p>');

                $h2Elem.text(data[i].name);
                $aElem.attr('id',data[i].id)
                    .attr('name', data[i].name)
                    .attr('class', 'ui-btn')
                    .attr('data-transition', 'none')
                    .attr('href', '#view-producer-group');

                     //$pElem.text(this.showCountry(data[i].pvcountries));
                    // $pElem.text(data[i].pvcountries);

                $aElem .append($h2Elem)
                    .append($pElem);

                $liElem
                .append($aElem);

                $('#producer_groups') .append($liElem);
            }
            $('#producer_groups').listview('refresh');

        },'json');
    } ,

//method to list all  buyers
showBuyers: function ()
    {
          $.get(Buyers,  function(data)
            {
                $('#buyer_list').html('');
                var len = data.buyers.length, i;

                for(i = 0; i < len; i++)
                {
                    var $pElem, $liElem, $h2Elem, $aElem;

                    $liElem = $('<li>');
                    $h2Elem = $('<h2>');
                    $aElem = $('<a>');
                    $pElem = $('<p>');

                    $h2Elem.text(data.buyers[i].name);
                    $aElem.attr('id',data.buyers[i].id)
                        .attr('name', data.buyers[i].name)
                        .attr('class', 'ui-btn')
                        .attr('data-transition', 'none')
                        .attr('href', '#view-buyer');

                    $pElem.text(data.buyers[i].country);

                    $aElem .append($h2Elem)
                        .append($pElem);

                    $aElem.bind('tap', function(){
                       //alert($(this).attr('id'));
                       VivoApi.ViewBuyer( $(this).attr('id'), $(this).attr('name'));
                    });

                    $liElem
                    .append($aElem);

                    $('#buyer_list') .append($liElem);
                }
                $('#buyer_list').listview('refresh');

            },'json');
        } ,
//method to view a producer
ViewBuyer: function( buyer_id ,name){

        $('#buyer-name').text('');
        $('#buyer-title').text(name);
        $('#contact-person').text('');
        $('#phone-one').text('');
        $('#phone-two').text('');
        $('#email').text('');
        $('#skype').text('');
        $('#address-one').text('');
        $('#address-two').text('');
        $('#address-three').text('');
        $('#buyer-country').text('');

        $.get(VivoApi.Buyers+'&single='+buyer_id, function(data)
        {
            $('#buyer-name').text(data.buyers[0].name);
            $('#contact-person').text(data.buyers[0].contactperson);
            $('#buyer-country').text(data.buyers[0].country);
            $('#phone-one').text(data.buyers[0].phoneone);
            $('#phone-two').text(data.buyers[0].phonetwo);
            $('#email').text(data.buyers[0].email);
            $('#skype').text(data.buyers[0].skype);
            $('#address-one').text(data.buyers[0].addressone);
            $('#address-two').text(data.buyers[0].addresstwo);
            $('#address-three').text(data.buyers[0].addressthree);

        },'json');
        $('#edit-buyer').bind('tap', function()
        {
           VivoApi.EditBuyer(buyer_id,name);
        });
},

//method to list all  buyer sales agreements
showBuyerSalesAgreements: function ()
    {

           $.get(this.BuyerSalesAgreement,  function(data)
            {
                $('#buyer_agreement_list').html('');
                var len = data.buyagrees.length, i;

                for(i = 0; i < len; i++)
                {
                    var $pElem, $liElem, $h2Elem, $aElem;

                    $liElem = $('<li>');
                    $h2Elem = $('<h2>');
                    $aElem = $('<a>');
                    $pElem = $('<p>');

                    $h2Elem.text(data.buyagrees[i].name);
                    $aElem.attr('id',data.buyagrees[i].id)
                        .attr('name', data.buyagrees[i].name)
                        .attr('class', 'ui-btn')
                        .attr('data-transition', 'none')
                        .attr('href', '#view-sales-agreement');

                         //$pElem.text(this.showCountry(data[i].pvcountries));
                        // $pElem.text(data[i].pvcountries);

                    $aElem .append($h2Elem)
                        .append($pElem);

                    $liElem
                    .append($aElem);

                    $('#buyer_agreement_list') .append($liElem);
                }
                $('#buyer_agreement_list').listview('refresh');

            },'json');
        } ,
//method to list all  Technicians
showTechnicians: function ()
    {

           $.get(this.Technician,  function(data)
            {
                $('#technicians_list').html('');
                var len = data.length, i;

                for(i = 0; i < len; i++)
                {
                    var $pElem, $liElem, $h2Elem, $aElem;

                    $liElem = $('<li>');
                    $h2Elem = $('<h2>');
                    $aElem = $('<a>');
                    $pElem = $('<p>');

                    $h2Elem.text(data[i].title.rendered);
                    $aElem.attr('id',data[i].id)
                        .attr('name', data[i].title.rendered)
                        .attr('class', 'ui-btn')
                        .attr('data-transition', 'none')
                        .attr('href', '#view-technician');

                         //$pElem.text(this.showCountry(data[i].pvcountries));
                        // $pElem.text(data[i].pvcountries);

                    $aElem .append($h2Elem)
                        .append($pElem);

                    $liElem
                    .append($aElem);

                    $('#technicians_list') .append($liElem);
                }
                $('#technicians_list').listview('refresh');

            },'json');
        } ,
//method to list all  plan vivos
showPlanVivos: function ()
      {
           $.get(this.PlanVivoInfo,  function(data)
            {

                $('#plan_vivo_list').html('');
                var len = data.pvs.length, i;
              //  alert(len);
                for(i = 0; i < len; i++)
                {

                    var $pElem, $liElem, $h2Elem, $aElem;

                    $liElem = $('<li>');
                    $h2Elem = $('<h2>');
                    $aElem = $('<a>');
                    $pElem = $('<p>');

                    $h2Elem.text(data.pvs[i].name);
                    $aElem.attr('id',data.pvs[i].id)
                        .attr('name', data.pvs[i].name)
                        .attr('class', 'ui-btn')
                        .attr('data-transition', 'none')
                        .attr('href', '#view-plan-vivo');

                    //$pElem.text(data.pvs[i].group);

                    $aElem .append($h2Elem)
                        .append($pElem);

                    $aElem.bind('tap', function(){
                       //alert($(this).attr('id'));
                       VivoApi.ViewPlanVivo( $(this).attr('id'), $(this).attr('name'));
                    });

                    $liElem
                    .append($aElem);

                    $('#plan_vivo_list') .append($liElem);
                }
                $('#plan_vivo_list').listview('refresh');

            },'json');
        } ,

//method to view a plan vivo
ViewPlanVivo: function( plan_vivo_id ,name)
{

                $('#plan-vivo-name').text('');
                $('#pv-title').text(name);
                /*$('#village').text('');
                $('#country').text('');
                $('#producer-group').text('');
                $('#community').text('');
                $('#province').text('');*/

                $.get(VivoApi.PlanVivoInfo+'&single='+plan_vivo_id, function(data)
                {
                    $('#plan-vivo-name').text(data.pvs[0].name);
                   /* $('#village').text(data.pvs[0].village);
                    $('#country').text(data.pvs[0].country);
                    $('#producer-group').text(data.pvs[0].group);
                    $('#community').text(data.pvs[0].community);
                    $('#province').text(data.pvs[0].province);*/

                },'json');
                $('#edit-plan-vivo').bind('tap', function()
                {
                   VivoApi.EditPlanVivo(plan_vivo_id,name);
                });
        },

formatAMPM: function (date) {
            var weekdays = new Array(7), day = null;
            weekdays[0] = "Sun";
            weekdays[1] = "Mon";
            weekdays[2] = "Tue";
            weekdays[3] = "Wed";
            weekdays[4] = "Thur";
            weekdays[5] = "Fri";
            weekdays[6] = "Sat";
        weekdays[6] = "Sat";

             var monthNames = new Array(12), month = null;
                    monthNames[0] = "Jan";
                    monthNames[1] = "Feb";
                    monthNames[2] = "Mar";
                    monthNames[3] = "Apr";
                    monthNames[4] = "May";
                    monthNames[5] = "Jun";
                    monthNames[6] = "Jul";
                    monthNames[7] = "Aug";
                    monthNames[8] = "Sep";
                    monthNames[9] = "Oct";
                    monthNames[10] = "Nov";
                    monthNames[11] = "Dec";


              var dat = date.getDate();
               var month = date.getMonth();
              var year = date.getFullYear();
              var hours = date.getHours();
              var minutes = date.getMinutes();
              var ampm = hours >= 12 ? 'pm' : 'am';
              hours = hours % 12;
              hours = hours ? hours : 12; // the hour '0' should be '12'
              minutes = minutes < 10 ? '0'+minutes : minutes;

             day = weekdays[date.getDay()];
             month = monthNames[date.getMonth()];
              var strTime = day + ' ' + dat  + ' ' + month  + ' ' +  year + ' '+ hours + ':' + minutes + ' ' + ampm;
              return strTime ;
            }
         // this method ends here

};