/*
*   Install the app
*       - Call register installation api method, add device info to the call. This will generate a token for the device.
*   Login with technician credentials
*       - And call the map installation api method which connects the user account with token.
*   Application initialization 
*       - Fetch district of technnician from cloud and insert it in phone database 
*       - Fetch subcounties of the district from cloud and insert them  in phone database
*       - Fetch villages of each suncounty from cloud and insert them in phone database
*       - Fetch Producer groups created by the technician from the cloud and insert them in phone database
*       - Fetch Producers created by the technician from cloud and insert them in the phone database
*       - Fetch Plan Vivos for approved producers created by the technician from the cloud to phone database
*   Load and display all data from phone database
*
*/

/*
* app version
*/
/*
* app version
*/
var $version = '0.0.1';

/*
* Api urls
*/
var $url ={
    inbound : 'http://ggrids.com/ecotrust/api/inbound',
    outbound : 'http://ggrids.com/ecotrust/api/outbound'
}

$installation_token = window.localStorage["token"];
$installation_id = window.localStorage["installation"];
$user_id	= window.localStorag["userid"];

function register_installation() {
		
		$.post(
			$url.inbound, {
				method: 'register_installation',
				version: $version,
				device_info: JSON.stringify( device )
			},
			function( $data ) {
				
				if ( $data.result == 'error' )
					show_error_alert( $data.message );
					
				else {
					$installation = {
						id: $data.data.id,
						token: $data.data.token,
						version: $version
					};
				
					window.localStorage.setItem('token', $installation.token );
					window.localStorage.setItem('id',  $installation.id );
					localStorage.setItem('app_version', $installation.version );
				//	window.localStorage.setItem( 'installation', JSON.stringify( $installation ) );
				}
			},
			'json'
		).fail( function() {
			show_error_alert( 'Please connect to the internet  !' );
			
			setTimeout( function() {
				register_installation();
				
			}, 2000 );
		});
	}


function init() {
    document.addEventListener("deviceready", deviceReady, true);
    bindButtons();
    setInterval(function() {
            showProducers(),
            ViewPlanVivo(),
            showProducerGroups()
            
        }, 5000);
    delete init;
}

function databaseConnection()
{
        return window.openDatabase("PlanVivo", "1.0", "PV DB", 2000000);
}

function checkPreAuth() {
    var form = $("#loginForm");
    if(window.localStorage["username"] != undefined && window.localStorage["password"] != undefined) {
        $( "#login-email", form ).val( window.localStorage["username"] );
        $( "#password", form ).val( window.localStorage["password"] );
       // alert('check pre auth');
        handleLogin();
    }
}
/*
* function for user log in to the applicetion
*/

function handleLogin() {
            
    var form = $("#loginForm"); 
    //disable the button so we can't resubmit while we wait
    $("#account-login", form ).attr("disabled","disabled" );
    
    var u = $( "#login-email", form).val();
    var p = $("#password", form).val();
   
    console.log( "click" );
   alert($installation_token +' id:'+ $installation_id);
      if(u != '' && p!= '') {
            $('#logging').popup('open');
                $.post(
                    $url.outbound, {
                        method: 'login',
                        token: $installation_token,
                        uname: u,
                        pwd: p
                    },
                    function( $data ) {
                        if ( $data.result == 'error' )
                            alert( $data.message );

                        else {
                             window.localStorage.setItem( "username", u );
                             window.localStorage.setItem( "password", p );
							 window.localStorage.setItem( "userid", $data.id );
                             map_installation();
                        //   $( ":mobile-pagecontainer" ).pagecontainer( "change", "#home", { transition: 'flip'} );
                        }
                         $("#submitButton").removeAttr("disabled");
                    },
                    'json'
                );
    
    } else {
        //Thanks !
        navigator.notification.alert("You must enter a username and password", function() {});
        $("#account-login").removeAttr("disabled");
    }
    return false;
}

/*
* function that maps auser to a device
*/
function map_installation(){
    
    $.post(
            $url.inbound,
            {
                method: 'map_installation',
                id: $installation_id,
                token: $installation_token,
                user: user_id
            },
            function( $data ) {	
            if ( $data.result == 'error' )
                show_error_alert( $data.message );

            else {
                 $( ":mobile-pagecontainer" ).pagecontainer( "change", "#home", { transition: 'flip'} );	
            }

            }, 'JSON');
}


function deviceReady() 
{
    if( $installation_id == '' && $installation_token == ' '){
         register_installation();
    }   
  //  alert($installation_id);
        //initialize login form
        $("#loginForm").on("submit",handleLogin);
        
        // make a database connection
        myDB = databaseConnection();
    
        //create tables
        myDB.transaction(function(tx) {
        //create table
            tx.executeSql("CREATE TABLE IF NOT EXISTS TECHNICIANS (id integer primary key, email text , username text, remote_user_id )");
            tx.executeSql("CREATE TABLE IF NOT EXISTS PRODUCER_GROUPS (id integer primary key  , group_name text,phone text, address text , district text ,sub_county text, village text ,technician_id, group_remote_id unique)");
            tx.executeSql("CREATE TABLE IF NOT EXISTS PRODUCERS (id integer primary key,first_name text, other_names text,gender text, producer_phone text,  producer_address , producer_district , producer_sub_county  ,producer_village , producer_bank , account_name , producer_bank_account  , belong_to_group, individual_producer  ,choose_producer_group, position_held , technician_id ,producer_remote_id unique)");
            tx.executeSql("CREATE TABLE IF NOT EXISTS DISTRICTS ( id integer primary key ,district_name text, remote_district_id )");
            tx.executeSql("CREATE TABLE IF NOT EXISTS SUBCOUNTIES ( id integer primary key ,county_name text, foreign_key_remote_district_id, remote_county_id )");
            tx.executeSql("CREATE TABLE IF NOT EXISTS VILLAGES ( id integer primary key ,county_name text, foreign_key_remote_county_id ,remote_village_id )");
           
        }, function(err){
            //errors for all transactions are reported here
            alert("Error: " + err.message);
        });
    
        
}

/*
* function that binds all buttons in application and adds events on them
*/
function bindButtons()
{
            
         
            // button to login
           /* $('#account-login').bind('tap', function(){
                var boolean = LoginValidate($('#login-email'),$('#password') ,$('#errors'));
                 if(boolean)
                {
                    controller.login($('#login-email'),$('#password') );
                }
            });*/
            // button to add producer group
            $('#button-add-producer-group').bind('tap', function()
            {
              var boolean = ProducerGroupsValidate($('#group-name') ,$('#group_phone_number') ,$('#group-address') ,$('#group-district') ,$('#group-sub-county') , $('#group-village')  ,$('#add-producer-group-errors'));
                
                if(boolean)
                {  
                  AddProducerGroup($('#group-name') ,$('#group_phone_number') ,$('#group-address') ,$('#group-district') ,$('#group-sub-county') , $('#group-village') );
                }
            });
            // button to add producer
            $('#add-producer-button').bind('tap', function()
            {
              var boolean = ProducerValidate($('#first_name'),$('#other_names'), $('#producer_phone_number'),$('#producer_address'), $('#producer_district') ,$('#producer_sub_county'),$('#producer_village') , $('#producer_bank') ,$('#account_name'), $('#producer_bank_account') ,$('#add-producer-errors'));
                if(boolean)
                {
                  AddProducer($('#first_name') ,$('#other_names') , $("input[name='gender']:checked").val(), $('#producer_phone_number'),$('#producer_address') , $('#producer_district') ,$('#producer_sub_county'), $('#producer_village') , $('#producer_bank') ,$('#account_name'),$('#producer_bank_account'),$('#belong_to_group') ,$('#individual') ,$('#choose_producer_group') , $('#position_held') );
                }
            });

           // button to add a technician
            $('#add-technician-button').bind('tap', function()
            {
              var boolean = TechnicianValidate($('#technician_first_name') ,$('#technician_other_names') ,$('#technician_title') ,$('#technician_address')  , $('#technician_village'), $('#technician_community'), $('#technician_state'), $('#technician_country')  ,$('#technician-errors'));
                if(boolean)
                {
                  AddTechnician($('#technician_first_name') ,$('#technician_other_names') ,$('#technician_title') ,$('#technician_address')  , $('#technician_village'), $('#technician_community'), $('#technician_state'), $('#technician_country') );
                }
            });
            
            // logout
            $('#logout').off('click tap').on('tap', function()
            {
              logout();
             //   technician();
            });
                
     }

/*
* function that logs out  users from the application
*/
function logout()
{

   //remove
    localStorage.removeItem("username");
    localStorage.removeItem("password");
    $( ":mobile-pagecontainer" ).pagecontainer( "change", "#loginPage", { transition: 'flip'} );
     $("#account-login").removeAttr("disabled");
}
