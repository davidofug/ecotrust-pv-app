var controller ={
    
 //databaseConnection: null;
// login method
login: function(email, password){

   
    $( ":mobile-pagecontainer" ).pagecontainer( "change", "#home", { transition: 'flip'} );
          $('#loging').popup('open');
          var errors = $('#errors');
          errors.attr('style','font-weight: none; color: #FFF; background: #61a93e; padding: 3px;')
              .text('')
              .hide();
    /*
    *   when a user submits his/her credentials 
    *   his/her credentials are sent to the server to compare with the online database
    *   then serve sends data to the phone which is stored in local storage and also stored in profile table
    *    then a session is started and the app flips opens
    */
        /*
        
        //post method to the server
         $.post(this.login_url, {email:email.val(), password:password.val()}, function(data)
          {
             // alert(data[0].email);
              if(data[0].email = email)
              {
                  $('#loging').popup('close');
                  email.val('');
                  password.val('');
                  errors.text('')
                      .hide();
                  
              }else
              {
                  $('#loging').popup('close');
                  errors.text(data.message)
                      .show();
              }
          },'json');
          
          //method to insert into the tables but am gona change it to store in localstorage 
        myDB.transaction(function(transaction) {
                            var executeQuery = "INSERT INTO SESSION ( email ,username) VALUES (?,?)";             
                            transaction.executeSql(executeQuery, [email,password]
                                , function(tx, result) {
                                    $( ":mobile-pagecontainer" ).pagecontainer( "change", "#home", { transition: 'flip'} );
                                     alert('Inserted');
                                },
                                function(error){
                                     alert('Error occurred'); 
                                });
                        });
        
      */
      },
//new login validation
LoginValidate: function(email, password, error){
        
    var errors = $('#errors');
          errors.attr('style','font-weight: none; color: #FFF; background: #61a93e; padding: 3px;')
              .hide();
      if(email.val() == '' || email.val() == null)
      {
          email.attr('style', 'border: none;');
          errors.text('Email is missing !')
              .show();
          email.attr('style', 'border: thin solid #61a93e;');
          return false;
      }else if(password.val() == '' || password.val() == null)
      {
          email.attr('style', 'border: none;');
          errors.text('Password is missing !')
              .show();
          password.attr('style', 'border: thin solid #61a93e;');
          return false;
      }else if (password.val().length < 6)
      {    
              email.attr('style', 'border: none;');
              errors.text('Password should have at-least 6 character!')
                  .show();
              password.attr('style', 'border: thin solid #61a93e;');
              return false;
      }else
      {

              errors.text('')
                  .hide();
              email.attr('style', 'border: none;');
              password.attr('style', 'border: none;');
              return true;

      }

    
  },
    
//method to validate a producer group
ProducerGroupsValidate: function ( name, phone, address, district, sub_county, village, errors)
{
//alert('ProducerGroupsValidate');
   var regexp = /^[a-zA-Z]+$/;
   var errors = $('#add-producer-group-errors');
   errors.attr('style','font-weight: none; color: #FFF; background: #61a93e; padding: 3px;')
       .hide();
   if(name.val() == '' || name.val() == null)
   {
       address.attr('style', 'border: none;');
       village.attr('style', 'border: none;');
       district.attr('style', 'border: none;');
       sub_county.attr('style', 'border: none;');
       errors.text('Name is missing!')
           .show();
       name.attr('style', 'border: thin solid #61a93e;');
       return false;
   }else if(phone.val() =='' || phone.val() == null  )
   {
       name.attr('style', 'border: none;');
       village.attr('style', 'border: none;');
       district.attr('style', 'border: none;');
       sub_county.attr('style', 'border: none;');
       errors.text('Phone number is missing')
           .show();
       phone.attr('style', 'border: thin solid #61a93e;');
       return false;
   }else if(address.val() =='' || address.val() == null  )
   {
       name.attr('style', 'border: none;');
       village.attr('style', 'border: none;');
       district.attr('style', 'border: none;');
       sub_county.attr('style', 'border: none;');
       errors.text('Address is missing')
           .show();
       address.attr('style', 'border: thin solid #61a93e;');
       return false;
   }else if(district.val() == '' || district.val() == null)
   {
      name.attr('style', 'border: none;');
      address.attr('style', 'border: none;');
      village.attr('style', 'border: none;');
      sub_county.attr('style', 'border: none;');
      errors.text('District is missing!')
          .show();
      district.attr('style', 'border: thin solid #61a93e');
      return false;
  }else if(sub_county.val() == '' || sub_county.val() == null)
   {
          name.attr('style', 'border: none;');
          village.attr('style', 'border: none;');
          district.attr('style', 'border: none;');
          address.attr('style', 'border: none;');
          errors.text('Sub county  is missing')
              .show();
          sub_county.attr('style', 'border: thin solid #61a93e');
          return false;
      }else if(village.val() == '' || village.val() == null)
   {
       name.attr('style', 'border: none;');
       address.attr('style', 'border: none;');
       district.attr('style', 'border: none;');
       sub_county.attr('style', 'border: none;');
       errors.text('Village is missing!')
           .show();
       village.attr('style', 'border: thin solid #61a93e');
       return false;
   }else
   {
       errors.text('')
           .hide();
       name.attr('style', 'border: none;');
       phone.attr('style', 'border: none;');
       village.attr('style', 'border: none;');
       district.attr('style', 'border: none;');
       sub_county.attr('style', 'border: none;');
       address.attr('style', 'border: none;');
       return true;
    }
},
  

// method to add producer group
AddProducerGroup: function(form_name , form_phone , form_address  ,form_district , form_sub_county,  form_village)
{
   // alert('am here inserting ');
    var address = form_address.val(),
       name = form_name.val(),
       village = form_village.val(),
       district = form_district.val(),
       phone = form_phone.val(),
       sub_county = form_sub_county.val();

        myDB.transaction(function(transaction) {
            var executeQuery ="INSERT INTO PRODUCER_GROUPS ( group_name ,phone, address , district ,sub_county, village ) VALUES(?,?,?,?,?,?)";
    ;             
            transaction.executeSql(executeQuery, [name , phone  ,  address  , district , sub_county  , village ]
                , function(tx, result) {
                    $( ":mobile-pagecontainer" ).pagecontainer( "change", "#pgroups", { transition: 'flip'} );
                     alert('Inserted');
                },
                function(error){
                     alert('Error occurred'); 
                });
        });

},
   
//method to validate a producer
ProducerValidate: function (first_name , other_name ,producer_phone_number , producer_address , producer_district , producer_sub_county , producer_village , producer_bank , account_name  , producer_bank_account , errors)
{

   var errors = $('#add-producer-errors');
   errors.attr('style','font-weight: none; color: #FFF; background: #61a93e; padding: 3px;')
       .hide();
   if(first_name.val() == '' || first_name.val() == null)
   {
       other_name.attr('style', 'border: none;');
       producer_phone_number.attr('style', 'border: none;');
       producer_address.attr('style', 'border: none;');
       producer_district.attr('style', 'border: none;');
       producer_sub_county.attr('style', 'border: none;');
       producer_village.attr('style', 'border: none;');
       producer_bank.attr('style', 'border: none;');
       account_name.attr('style', 'border: none;');
       producer_bank_account.attr('style', 'border: none;');
       errors.text('First name is missing!')
           .show();
       first_name.attr('style', 'border: thin solid #61a93e;');
       return false;
   }else if(other_name.val() =='' || other_name.val() == null  )
   {
      first_name.attr('style', 'border: none;');
      producer_phone_number.attr('style', 'border: none;');
      producer_address.attr('style', 'border: none;');
      producer_district.attr('style', 'border: none;');
      producer_sub_county.attr('style', 'border: none;');
      producer_village.attr('style', 'border: none;');
      producer_bank.attr('style', 'border: none;');
      account_name.attr('style', 'border: none;');
      producer_bank_account.attr('style', 'border: none;');
      errors.text('Other names is missing!')
             .show();
      other_name.attr('style', 'border: thin solid #61a93e;');
         return false;
   }else if(producer_phone_number.val() == '' || producer_phone_number.val() == null)
   {
         other_name.attr('style', 'border: none;');
         first_name.attr('style', 'border: none;');
         producer_address.attr('style', 'border: none;');
         producer_district.attr('style', 'border: none;');
         producer_sub_county.attr('style', 'border: none;');
         producer_village.attr('style', 'border: none;');
         producer_bank.attr('style', 'border: none;');
         account_name.attr('style', 'border: none;');
         producer_bank_account.attr('style', 'border: none;');
         errors.text('Phone number is missing!')
            .show();
         producer_phone_number.attr('style', 'border: thin solid #61a93e;');
        return false;
   }else if(producer_address.val() == '' || producer_address.val() == null)
   {
           other_name.attr('style', 'border: none;');
           first_name.attr('style', 'border: none;');
           producer_phone_number.attr('style', 'border: none;');
           producer_district.attr('style', 'border: none;');
           producer_sub_county.attr('style', 'border: none;');
           producer_village.attr('style', 'border: none;');
           producer_bank.attr('style', 'border: none;');
           account_name.attr('style', 'border: none;');
           producer_bank_account.attr('style', 'border: none;');
           errors.text('Address is missing!')
               .show();
          producer_address.attr('style', 'border: thin solid ##61a93e;');
          return false;
   }else if(producer_district.val() == '' || producer_district.val() == null)
   {
           other_name.attr('style', 'border: none;');
           first_name.attr('style', 'border: none;');
           producer_phone_number.attr('style', 'border: none;');
           producer_address.attr('style', 'border: none;');
           producer_sub_county.attr('style', 'border: none;');
           producer_village.attr('style', 'border: none;');
           producer_bank.attr('style', 'border: none;');
           account_name.attr('style', 'border: none;');
           producer_bank_account.attr('style', 'border: none;');
          errors.text('District is missing!')
              .show();
          producer_district.attr('style', 'border: thin solid #61a93e;');
          return false;
   }else if(producer_sub_county.val() == '' || producer_sub_county.val() == null)
   {
             other_name.attr('style', 'border: none;');
              first_name.attr('style', 'border: none;');
              producer_phone_number.attr('style', 'border: none;');
              producer_address.attr('style', 'border: none;');
              producer_district.attr('style', 'border: none;');
              producer_village.attr('style', 'border: none;');
              producer_bank.attr('style', 'border: none;');
              account_name.attr('style', 'border: none;');
              producer_bank_account.attr('style', 'border: none;');
             errors.text('Sub county is missing!')
                 .show();
             producer_sub_county .attr('style', 'border: thin solid #61a93e;');
             return false;
   }else if(producer_village.val() == '' || producer_village.val() == null)
   {
              other_name.attr('style', 'border: none;');
              first_name.attr('style', 'border: none;');
              producer_phone_number.attr('style', 'border: none;');
              producer_address.attr('style', 'border: none;');
              producer_district.attr('style', 'border: none;');
              producer_sub_county.attr('style', 'border: none;');
              producer_bank.attr('style', 'border: none;');
              account_name.attr('style', 'border: none;');
              producer_bank_account.attr('style', 'border: none;');
             errors.text('Village is missing!')
                 .show();
             producer_village.attr('style', 'border: thin solid #61a93e;');
             return false;
   }else if(producer_bank.val() == '' || producer_bank.val() == null)
   {
              other_name.attr('style', 'border: none;');
              first_name.attr('style', 'border: none;');
              producer_phone_number.attr('style', 'border: none;');
              producer_address.attr('style', 'border: none;');
              producer_district.attr('style', 'border: none;');
              producer_sub_county.attr('style', 'border: none;');
              producer_village.attr('style', 'border: none;');
              account_name.attr('style', 'border: none;');
              producer_bank_account.attr('style', 'border: none;');
             errors.text('Bank account is missing!')
                 .show();
             producer_bank.attr('style', 'border: thin solid #61a93e;');
             return false;
   }else if(account_name.val() == '' || account_name.val() == null)
   {
              other_name.attr('style', 'border: none;');
              first_name.attr('style', 'border: none;');
              producer_phone_number.attr('style', 'border: none;');
              producer_address.attr('style', 'border: none;');
              producer_district.attr('style', 'border: none;');
              producer_sub_county.attr('style', 'border: none;');
              producer_village.attr('style', 'border: none;');
              producer_bank.attr('style', 'border: none;');
              producer_bank_account.attr('style', 'border: none;');
              errors.text('Account name is missing!')
                 .show();
             account_name.attr('style', 'border: thin solid #61a93e;');
             return false;
   }else if(producer_bank_account.val() == '' || producer_bank_account.val() == null)
   {
              other_name.attr('style', 'border: none;');
              first_name.attr('style', 'border: none;');
              producer_phone_number.attr('style', 'border: none;');
              producer_address.attr('style', 'border: none;');
              producer_district.attr('style', 'border: none;');
              producer_sub_county.attr('style', 'border: none;');
              producer_village.attr('style', 'border: none;');
              producer_bank.attr('style', 'border: none;');
              account_name.attr('style', 'border: none;');
              errors.text('Account number is missing!')
                 .show();
             producer_bank_account.attr('style', 'border: thin solid #61a93e;');
             return false;
   }else
   {
      errors.text('')
           .hide();

      first_name.attr('style', 'border: none;');
      other_name.attr('style', 'border: none;');
      producer_phone_number.attr('style', 'border: none;');
      producer_address.attr('style', 'border: none;');
      producer_bank.attr('style', 'border: none;');
      producer_bank_account.attr('style', 'border: none;');
      producer_village.attr('style', 'border: none;');
      producer_district.attr('style', 'border: none;');
      producer_sub_county.attr('style', 'border: none;');
      producer_bank.attr('style', 'border: none;');
      account_name.attr('style', 'border: none;');
      producer_bank_account.attr('style', 'border: none;');

       return true;

       }
},
   //method to add a producer
AddProducer: function (first_name, other_names, gender, producer_phone_number, producer_address, producer_district, producer_sub_county, producer_village ,producer_bank ,account_name,producer_bank_account,belong_to_group,individual,choose_producer_group,position_held )
{
  myDB.transaction(function(transaction) {
        var executeQuery ="INSERT INTO PRODUCERS ( first_name,other_names,gender,producer_phone_number,producer_address,producer_district,producer_sub_county,producer_village ,producer_bank ,account_name,producer_bank_account,belong_to_group,individual_producer,choose_producer_group,position_held ) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
;             
        transaction.executeSql(executeQuery, [first_name.val(),other_names.val(),gender.val(),producer_phone_number.val(),producer_address.val(),producer_district.val(),producer_sub_county.val(),producer_village.val() ,producer_bank.val() ,account_name.val(), producer_bank_account.val(), belong_to_group.val(),individual.val(),choose_producer_group.val(),position_held.val() ]
            , function(tx, result) {
                $( ":mobile-pagecontainer" ).pagecontainer( "change", "#producers", { transition: 'flip'} );
                 alert('Inserted');
            },
            function(error){
                 alert('Error occurred'); 
            });
    });   
}  

    
};