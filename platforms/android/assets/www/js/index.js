/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
      /*  document.addEventListener("online", this.whenOnline, false);
        document.addEventListener("offline", this.whenOffline, false);
      */
         this.bindButtons();
        setInterval(function() {
            api.showProducers(),
            api.showPlanVivos(),
            api.showProducerGroups(),
            api.showBuyers(),
            api.showBuyerSalesAgreements(),
            api.showTechnicians()
        }, 2000);

    },

    databaseConnection: function(){
        return window.sqlitePlugin.openDatabase({name: "mySQLite.db", location: 'default'});
    },
    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        
          
            myDB = this.databaseConnection();
            myDB.transaction(function(tx) {
            //create table
            tx.executeSql("CREATE TABLE IF NOT EXISTS SESSION ( user_id unique ,email text , username text, token text)");
            tx.executeSql("CREATE TABLE IF NOT EXISTS PRODUCER_GROUPS (id integer primary key  , group_name text,phone text, address text , district text ,sub_county text, village text ,group_remote_id unique)");
            tx.executeSql("CREATE TABLE IF NOT EXISTS PRODUCERS (id integer primary key,first_name text, other_names text,gender text, producer_phone text,  producer_address , producer_district , producer_sub_county  ,producer_village , producer_bank , account_name , producer_bank_account  , belong_to_group, individual_producer  ,choose_producer_group, position_held , producer_remote_id unique)");
            tx.executeSql("CREATE TABLE IF NOT EXISTS DISTRICTS ( id integer primary key ,district_name text, remote_district_id )");
            tx.executeSql("CREATE TABLE IF NOT EXISTS SUBCOUNTIES ( id integer primary key ,county_name text, foreign_key_remote_district_id, remote_county_id )");
            tx.executeSql("CREATE TABLE IF NOT EXISTS VILLAGES ( id integer primary key ,county_name text, foreign_key_remote_county_id ,remote_village_id )");
         //    alert("created all table from index.js ");
        }, function(err){
            //errors for all transactions are reported here
            alert("Error: " + err.message);
        });
       
        this.receivedEvent('deviceready');
    },
    
    bindButtons: function()
        {
            
            // button to login
            $('#account-login').bind('tap', function(){
                var boolean = controller.LoginValidate($('#login-email'),$('#password') ,$('#errors'));
                 if(boolean)
                {
                    controller.login($('#login-email'),$('#password') );
                }
            });
            // button to add producer group
            $('#button-add-producer-group').bind('tap', function()
            {
              var boolean = controller.ProducerGroupsValidate($('#group-name') ,$('#group_phone_number') ,$('#group-address') ,$('#group-district') ,$('#group-sub-county') , $('#group-village')  ,$('#add-producer-group-errors'));
                
                if(boolean)
                {  
                  controller.AddProducerGroup($('#group-name') ,$('#group_phone_number') ,$('#group-address') ,$('#group-district') ,$('#group-sub-county') , $('#group-village') );
                }
            });
            // button to add producer
            $('#add-producer-button').bind('tap', function()
            {
              var boolean = controller.ProducerValidate($('#first_name'),$('#other_names'),$('#producer_phone_number'),$('#producer_address'), $('#producer_district') ,$('#producer_sub_county'),$('#producer_village') , $('#producer_bank') ,$('#account_name'), $('#producer_bank_account') ,$('#add-producer-errors'));
                if(boolean)
                {
                  controller.AddProducer($('#first_name') ,$('#other_names') ,$("input[name='gender']:checked").val() ,$('#producer_phone_number'),$('#producer_address') , $('#producer_district') ,$('#producer_sub_county'), $('#producer_village') , $('#producer_bank') ,$('#account_name'),$('#producer_bank_account'),$('#belong_to_group') ,$('#individual') ,$('#choose_producer_group') , $('#position_held') );
                }
            });

           // button to add a technician
            $('#add-technician-button').bind('tap', function()
            {
              var boolean = controller.TechnicianValidate($('#technician_first_name') ,$('#technician_other_names') ,$('#technician_title') ,$('#technician_address')  , $('#technician_village'), $('#technician_community'), $('#technician_state'), $('#technician_country')  ,$('#technician-errors'));
                if(boolean)
                {
                  controller.AddTechnician($('#technician_first_name') ,$('#technician_other_names') ,$('#technician_title') ,$('#technician_address')  , $('#technician_village'), $('#technician_community'), $('#technician_state'), $('#technician_country') );
                }
            });


     },
  
    
    whenOnline: function(){
        //code will go here
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};



app.initialize();