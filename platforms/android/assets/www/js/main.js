var ProducerGroups= "http://ggrids.com/ecotrust/wp-json/wp/v2/producer-groups",
    Producers= "http://ggrids.com/ecotrust/api/outbound?method=get_producers&token=528b551ae9597e2486fe87676684d3d2&account=2",
    Technician= "http://ggrids.com/ecotrust/wp-json/wp/v2/pvtechnicians",
    PlanVivoInfo = "http://ggrids.com/ecotrust/api/outbound?method=get_pvs&token=528b551ae9597e2486fe87676684d3d2&account=2";

/*
* app version
*/
var $version = '0.0.1';

/*
* Api urls
*/
var $url ={
    inbound : 'http://ggrids.com/ecotrust/api/inbound',
    outbound : 'http://ggrids.com/ecotrust/api/outbound'
}

$installation_token = window.localStorage.getItem( 'token' );
$installation_id = window.localStorage.getItem( 'id' );
$user_id = window.localStorage.getItem( 'user_id' );

function register_installation() {
		//alert( "Registering installation" );
		$.post(
			$url.inbound, {
				method: 'register_installation',
				component: 'mobile',
				version: $version,
				device_info: JSON.stringify( device )
			},
			function( $data ) {
				if ( $data.result == 'error' )
					show_error_alert( $data.message );
					
				else {
					$installation = {
						id: $data.data.id,
						token: $data.data.token,
						version: $version
					};
				
					window.localStorage.setItem( 'token',  $installation.token );
					window.localStorage.setItem( 'id',  $installation.id);
				//	window.localStorage.setItem( 'installation', JSON.stringify( $installation ) );
				}
			},
			'json'
		).fail( function() {
			show_error_alert( 'Please connect to the internet  !' );
			
			setTimeout( function() {
				
				register_installation();
			}, 2000 );
		});
	}


function init() {
    document.addEventListener("deviceready", deviceReady, true);
    bindButtons();
    setInterval(function() {
            showProducers(),
            ViewPlanVivo(),
            showProducerGroups()
            
        }, 5000);
    delete init;
}

function databaseConnection()
{
        return window.openDatabase("PlanVivo", "1.0", "PV DB", 2000000);
}

function checkPreAuth(){
	
    var form = $("#loginForm");
	
    if( window.localStorage["username"] != undefined && window.localStorage["password"] != undefined ) {
		$("#login-email", form).val( window.localStorage["username"]);
        $("#password", form).val(window.localStorage["password"]);
		
       // alert('check pre auth');
	   
        handleLogin();
    }
}
/*
* function for user log in to the applicetion
*/

function handleLogin(){
            
    var form = $("#loginForm"); 
    //disable the button so we can't resubmit while we wait
    $("#account-login",form ).attr("disabled","disabled");
    
    var u = $("#login-email", form ).val();
    var p = $("#password", form ).val();
   
		if(u != '' && p != '') {
			
            $('#logging').popup( 'open' );
			
                $.post(
                    $url.outbound, {
                        method: 'login',
                        token: $installation_token,
                        uname: u,
                        pwd: p
                    },
                    function( $data ) {
                        if ( $data.result == 'error' )
                            show_error_alert( $data.message );

                        else {
                             window.localStorage["username"] = u;
                             window.localStorage["password"] = p;
							 window.localStorage["user_id"]  = $data.user.id;
                             map_installation();
                        $( ":mobile-pagecontainer" ).pagecontainer( "change", "#home", { transition: 'fade'} );
                        }
                         $("#submitButton").removeAttr("disabled");
                    },
                    'json'
                );
    
    } else {
        //Thanks !
        navigator.notification.alert("You must enter a username and password", function() {});
        $("#account-login").removeAttr("disabled");
    }
    return false;
}

/*
* function that maps auser to a device
*/
function map_installation(){
	//alert( "Mapping the installation" );
    $.post(
            $url.inbound,
            {
                method: 'map_installation',
                id: $installation_id,
                token: $installation_token,
                user: $user_id
            },
            function( $data ) {	
            if ( $data.result == 'error' )
				//alert( $data.message );
                show_error_alert( $data.message );

            else {
                 $( ":mobile-pagecontainer" ).pagecontainer( "change", "#home", { transition: 'flip'} );	
            }

            }, 'JSON');
}

/*
function handleLogin() {
    var form = $("#loginForm"); 
    //disable the button so we can't resubmit while we wait
    $("#account-login",form).attr("disabled","disabled");
    var u = $("#login-email", form).val();
    var p = $("#password", form).val();
    var user_id = 3;
    console.log("click");
    if(u != '' && p!= '') {
                window.localStorage["username"] = u;
                window.localStorage["password"] = p; 
        $( ":mobile-pagecontainer" ).pagecontainer( "change", "#home", { transition: 'flip'} );
             
        /*
        myDB.transaction(function(transaction) {
        var executeQuery = "INSERT INTO TECHNICIANS (email, username , remote_user_id) VALUES (?,?,?)";             
        transaction.executeSql(executeQuery, [p,u,user_id]
            , function(tx, result) {
                 alert('Inserted');
                $( ":mobile-pagecontainer" ).pagecontainer( "change", "#home", { transition: 'flip'} );
            },
            function(error){
                 alert('Error occurred'); 
            });
    });
       
       
        $.post("http://www.coldfusionjedi.com/demos/2011/nov/10/service.cfc?method=login&returnformat=json", {username:u,password:p}, function(res) {
            if(res == true) {
                //store
                window.localStorage["username"] = u;
                window.localStorage["password"] = p;                    
                $.mobile.changePage("some.html");
            } else {
                navigator.notification.alert("Your login failed", function() {});
            }
            $("#submitButton").removeAttr("disabled");
        },"json");*
    } else {
        //Thanks !
        navigator.notification.alert("You must enter a username and password", function() {});
        $("#account-login").removeAttr("disabled");
    }
    return false;
}
*/

function deviceReady(){
    if( $installation_id == null || $installation_token == null ){
    
		register_installation();
    }else{
		
        //initialize login form
        $("#loginForm").on("submit", handleLogin );
        
        // make a database connection
        myDB = databaseConnection();
    
        //create tables
        myDB.transaction(function(tx) {
        //create table
            tx.executeSql("CREATE TABLE IF NOT EXISTS TECHNICIANS (id integer primary key, email text , username text, remote_user_id )");
            tx.executeSql("CREATE TABLE IF NOT EXISTS PRODUCER_GROUPS (id integer primary key  , group_name text,phone text, address text , district text ,sub_county text, village text ,technician_id, group_remote_id unique)");
            tx.executeSql("CREATE TABLE IF NOT EXISTS PRODUCERS (id integer primary key,first_name text, other_names text,gender text, producer_phone text,  producer_address , producer_district , producer_sub_county  ,producer_village , producer_bank , account_name , producer_bank_account  , belong_to_group, individual_producer  ,choose_producer_group, position_held , technician_id ,producer_remote_id unique)");
            tx.executeSql("CREATE TABLE IF NOT EXISTS DISTRICTS ( id integer primary key ,district_name text, remote_district_id )");
            tx.executeSql("CREATE TABLE IF NOT EXISTS SUBCOUNTIES ( id integer primary key ,county_name text, foreign_key_remote_district_id, remote_county_id )");
            tx.executeSql("CREATE TABLE IF NOT EXISTS VILLAGES ( id integer primary key ,county_name text, foreign_key_remote_county_id ,remote_village_id )");
			tx.executeSql("CREATE TABLE IF NOT EXISTS USER ( id integer primary key , username text, password text)");
			
        }, function(err){
            //errors for all transactions are reported here
            alert("Error: " + err.message);
        });
    
        // gps
        ///document.getElementById("getPosition").addEventListener("click", getPosition);
        //document.getElementById("watchPosition").addEventListener("click", watchPosition);	
       // document.getElementById("cordovaDevice").addEventListener("click", sti);	
	   
	}

}

function bindButtons(){
            
         
            // button to login
           /* $('#account-login').bind('tap', function(){
                var boolean = LoginValidate($('#login-email'),$('#password') ,$('#errors'));
                 if(boolean)
                {
                    controller.login($('#login-email'),$('#password') );
                }
            });*/
            // button to add producer group
            $('#button-add-producer-group').bind('tap', function(){
              var boolean = ProducerGroupsValidate($('#group-name') ,$('#group_phone_number') ,$('#group-address') ,$('#group-district') ,$('#group-sub-county') , $('#group-village')  ,$('#add-producer-group-errors'));
                
                if(boolean)
                {  
                  AddProducerGroup($('#group-name') ,$('#group_phone_number') ,$('#group-address') ,$('#group-district') ,$('#group-sub-county') , $('#group-village') );
                }
            });
            // button to add producer
            $('#add-producer-button').bind('tap', function(){
              var boolean = ProducerValidate($('#first_name'),$('#other_names'), $('#producer_phone_number'),$('#producer_address'), $('#producer_district') ,$('#producer_sub_county'),$('#producer_village') , $('#producer_bank') ,$('#account_name'), $('#producer_bank_account') ,$('#add-producer-errors'));
                if(boolean)
                {
                  AddProducer($('#first_name') ,$('#other_names') , $("input[name='gender']:checked").val(), $('#producer_phone_number'),$('#producer_address') , $('#producer_district') ,$('#producer_sub_county'), $('#producer_village') , $('#producer_bank') ,$('#account_name'),$('#producer_bank_account'),$('#belong_to_group') ,$('#individual') ,$('#choose_producer_group') , $('#position_held') );
                }
            });

           // button to add a technician
            $('#add-technician-button').bind('tap', function(){
				
              var boolean = TechnicianValidate($('#technician_first_name') ,$('#technician_other_names') ,$('#technician_title') ,$('#technician_address')  , $('#technician_village'), $('#technician_community'), $('#technician_state'), $('#technician_country')  ,$('#technician-errors'));
                if(boolean){
                  AddTechnician($('#technician_first_name') ,$('#technician_other_names') ,$('#technician_title') ,$('#technician_address')  , $('#technician_village'), $('#technician_community'), $('#technician_state'), $('#technician_country') );
                }
            });
            
            // logout
            $('#logout').off('click tap').on('tap', function()
            {
              logout();
             
            });
    
            // cordovaDevice
            $('#cordovaDevice').bind('tap', function()
            {
              cordovaDevice();
            
            });
                
     }

function logout(){

   //remove
    localStorage.removeItem("username");
    localStorage.removeItem("password");
    $( ":mobile-pagecontainer" ).pagecontainer( "change", "#loginPage", { transition: 'fade'} );
     $("#account-login").removeAttr("disabled");
}

function technician(){
    myDB.transaction(function(transaction) {
           transaction.executeSql('SELECT * FROM TECHNICIANS', [], function (tx, results) {
           var id =  results.rows.item(0).remote_user_id;
            return id;
      }, null);
  });
}
 
//method to list all  producers
 function showProducers (){
        
    //  alert('len');
         //  $.get(Producers,  function(data)
         //   {
           myDB.transaction(function(transaction) {
           transaction.executeSql('SELECT * FROM PRODUCERS ORDER BY id DESC', [], function (tx, results) {
     
                $('#producer_list').html('');
                var len = results.rows.length, i;

                for(i = 0; i < len; i++)
                {

                    var $pElem, $liElem, $h2Elem, $aElem ,$imgElem;

                    $liElem = $('<li>');
                    $h2Elem = $('<h2>');
                    $aElem = $('<a>');
                    $pElem = $('<p>');
                    $imgElem = $('<img>');
                    
                    $imgElem.attr('src', '//lorempixel.com/150/150/people/1/')
                            .attr('class', 'ui-thumbnail ui-thumbnail-circular');
                    $h2Elem.text(results.rows.item(i).first_name + ' '+results.rows.item(i).other_names);
                    $aElem.attr('id',results.rows.item(i).id)
                        .attr('remote_id',results.rows.item(i).producer_remote_id)
                        .attr('name', results.rows.item(i).first_name +' '+results.rows.item(i).other_names)
                        .attr('class', 'ui-btn')
                        .attr('data-transition', 'none')
                        .attr('href', '#view-producer');

                    $pElem.text(results.rows.item(i).producer_village +' '+results.rows.item(i).producer_sub_county +' '+results.rows.item(i).producer_district);

                    $aElem.append($imgElem)
                        .append($h2Elem)
                        .append($pElem);

                    $aElem.bind('tap', function(){
                       //alert($(this).attr('id'));
                       ViewProducer( $(this).attr('id'), $(this).attr('name'));
                    });

                    $liElem
                    .append($aElem);

                    $('#producer_list') .append($liElem);
                }
            
                $('#producer_list').listview('refresh');
            }, null);
          });

         //   },'json');
        } 

//method to view a producer
 function ViewProducer( producer_id ,name){

        $('#producer-name').text('');
        $('#producer-title').text(name);
        $('#producer-village').text('');
        $('#producer-district').text('');
        $('#producer-group').text('');
        $('#producer-subcounty').text('');
        $('#producer-gender').text('');
        $('#producer-address').text('');
        $('#producer-phone').text('');
        $('#position-held').text('');
        $('#producer-bank').text('');
        $('#producer-account-name').text('');
        $('#producer-account-no').text('');
        $('#belong_to_group').text('');
        $('#producer-individual').text('');
        

    //    $.get(this.Producers+'&single='+producer_id, function(data)
       // {
       myDB.transaction(function(transaction) {
           transaction.executeSql('SELECT * FROM PRODUCERS WHERE id =?', [producer_id], function (tx, results) {
     
            $('#producer-name').text(results.rows.item(0).first_name +' ' + results.rows.item(0).other_names );
            $('#producer-gender').text(results.rows.item(0).gender);
            $('#producer-village').text(results.rows.item(0).producer_village);
            $('#producer-address').text(results.rows.item(0).producer_address);
            $('#producer-phone').text(results.rows.item(0).producer_phone);
            $('#producer-subcounty').text(results.rows.item(0).producer_sub_county);
            $('#producer-group').text(results.rows.item(0).choose_producer_group);
            $('#position-held').text(results.rows.item(0).position_held);
            $('#producer-district').text(results.rows.item(0).producer_district);
            $('#producer-bank').text(results.rows.item(0).producer_bank);
            $('#producer-account-name').text(results.rows.item(0).account_name);
            $('#producer-account-no').text(results.rows.item(0).producer_bank_account);
            $('#belong_to_group').text(results.rows.item(0).belong_to_group);
            $('#producer-individual').text(results.rows.item(0).individual_producer);
               
             }, null);
          });

        //},'json');
        $('#edit-producer').bind('tap', function(){
           EditProducer(producer_id,name);
        });
}

//method to list all  producer groups
 function showProducerGroups(){

     //  $.get(this.ProducerGroups,  function(data)
       // {
      myDB.transaction(function(transaction) {
           transaction.executeSql('SELECT * FROM PRODUCER_GROUPS ORDER BY id DESC', [], function (tx, results) {
  
           
            $('#producer_groups').html('');
            var len = results.rows.length, i;

            for(i = 0; i < len; i++)
            {
                var $pElem, $liElem, $h2Elem, $aElem , $p2Elem, $p3Elem, $p4Elem;

                $liElem = $('<li>');
                $h2Elem = $('<h2>');
                $aElem = $('<a>');
                $pElem = $('<p>');
                $p2Elem = $('<p>');
                p3Elem = $('<p>');
                p4Elem = $('<p>');

                $h2Elem.text(results.rows.item(i).group_name);
                $aElem.attr('id',results.rows.item(i).id)
                    .attr('name', results.rows.item(i).group_name)
                    .attr('class', 'ui-btn')
                    .attr('data-transition', 'none')
                    .attr('href', '#view-producer-group');

                     $pElem.text(results.rows.item(i).village +' '+ results.rows.item(i).sub_county +' '+results.rows.item(i).district);

                $aElem .append($h2Elem)
                    .append($pElem);
                $aElem.bind('tap', function(){
                   //alert($(this).attr('id'));
                    ViewProducerGroup( $(this).attr('id'), $(this).attr('name'));
                });
                $liElem
                .append($aElem);

                $('#producer_groups') .append($liElem);
            }
            $('#producer_groups').listview('refresh');
               
           }, null);
          });


       // },'json');
    } 

//method to view a producer
 function ViewProducerGroup( group_id ,name){

        $('#group_name').text('');
        $('#group-title').text(name);
        $('#village').text('');
        $('#district').text('');
        $('#address').text('');
        $('#sub_county').text('');
        $('#phone').text('');
        

    //    $.get(this.Producers+'&single='+producer_id, function(data)
       // {
      myDB.transaction(function(transaction) {
           transaction.executeSql('SELECT * FROM PRODUCER_GROUPS WHERE id =?', [group_id],
              function (tx, results) {
     
            $('#group_name').text(results.rows.item(0).group_name);
            $('#village').text(results.rows.item(0).village);
            $('#sub_county').text(results.rows.item(0).sub_county);
            $('#address').text(results.rows.item(0).address);
            $('#district').text(results.rows.item(0).district);
            $('#phone').text(results.rows.item(0).phone);
            
           }, null);
          });

        //},'json');
        $('#edit-producer').bind('tap', function(){
           EditProducerGroup(producer_id,name);
        });
}

//method to view a plan vivo
 function ViewPlanVivo( plan_vivo_id ,name){

                $('#plan-vivo-name').text('');
                $('#pv-title').text(name);
                /*$('#village').text('');
                $('#country').text('');
                $('#producer-group').text('');
                $('#community').text('');
                $('#province').text('');*/

                $.get(this.PlanVivoInfo+'&single='+plan_vivo_id, function(data)
                {
                   /* $('#plan-vivo-name').text(data.pvs[0].name);
                    $('#village').text(data.pvs[0].village);
                    $('#country').text(data.pvs[0].country);
                    $('#producer-group').text(data.pvs[0].group);
                    $('#community').text(data.pvs[0].community);
                    $('#province').text(data.pvs[0].province);*/

                },'json');
                $('#edit-plan-vivo').bind('tap', function()
                {
                   this.EditPlanVivo(plan_vivo_id,name);
                });
    }

//method to validate a producer group
function ProducerGroupsValidate ( name, phone, address, district, sub_county, village, errors){
//alert('ProducerGroupsValidate');
   var regexp = /^[a-zA-Z]+$/;
   var errors = $('#add-producer-group-errors');
   errors.attr('style','font-weight: none; color: #FFF; background: #f00; padding: 3px;')
       .hide();
   if(name.val() == '' || name.val() == null)
   {
       address.attr('style', 'border: none;');
       village.attr('style', 'border: none;');
       district.attr('style', 'border: none;');
       sub_county.attr('style', 'border: none;');
       errors.text('Name is missing!')
           .show();
       name.attr('style', 'border: thin solid #f00;');
       return false;
   }else if(phone.val() =='' || phone.val() == null  )
   {
       name.attr('style', 'border: none;');
       village.attr('style', 'border: none;');
       district.attr('style', 'border: none;');
       sub_county.attr('style', 'border: none;');
       errors.text('Phone number is missing')
           .show();
       phone.attr('style', 'border: thin solid #f00;');
       return false;
   }else if(address.val() =='' || address.val() == null  )
   {
       name.attr('style', 'border: none;');
       village.attr('style', 'border: none;');
       district.attr('style', 'border: none;');
       sub_county.attr('style', 'border: none;');
       errors.text('Address is missing')
           .show();
       address.attr('style', 'border: thin solid #f00;');
       return false;
   }else if(district.val() == '' || district.val() == null)
   {
      name.attr('style', 'border: none;');
      address.attr('style', 'border: none;');
      village.attr('style', 'border: none;');
      sub_county.attr('style', 'border: none;');
      errors.text('District is missing!')
          .show();
      district.attr('style', 'border: thin solid #f00');
      return false;
  }else if(sub_county.val() == '' || sub_county.val() == null)
   {
          name.attr('style', 'border: none;');
          village.attr('style', 'border: none;');
          district.attr('style', 'border: none;');
          address.attr('style', 'border: none;');
          errors.text('Sub county  is missing')
              .show();
          sub_county.attr('style', 'border: thin solid #f00');
          return false;
      }else if(village.val() == '' || village.val() == null)
   {
       name.attr('style', 'border: none;');
       address.attr('style', 'border: none;');
       district.attr('style', 'border: none;');
       sub_county.attr('style', 'border: none;');
       errors.text('Village is missing!')
           .show();
       village.attr('style', 'border: thin solid #f00');
       return false;
   }else
   {
       errors.text('')
           .hide();
       name.attr('style', 'border: none;');
       phone.attr('style', 'border: none;');
       village.attr('style', 'border: none;');
       district.attr('style', 'border: none;');
       sub_county.attr('style', 'border: none;');
       address.attr('style', 'border: none;');
       return true;
    }
}
// method to add producer group
function AddProducerGroup (form_name , form_phone , form_address  ,form_district , form_sub_county,  form_village){
    $('#pgroup-pop').popup('open');
            var errors = $('#add-producer-group-errors');
            errors.attr('style','font-weight: none; color: #FFF; background: #F00; padding: 3px;')
                .text('')
                .hide();
   // alert('am here inserting ');
    var address = form_address.val(),
       name = form_name.val(),
       village = form_village.val(),
       district = form_district.val(),
       phone = form_phone.val(),
       sub_county = form_sub_county.val();

        myDB.transaction(function(transaction) {
            var executeQuery ="INSERT INTO PRODUCER_GROUPS ( group_name ,phone, address , district ,sub_county, village ,technician_id ) VALUES(?,?,?,?,?,?,?)";
                 
            transaction.executeSql(executeQuery, [name , phone  ,  address  , district , sub_county  , village ,technician_id]
                , function(tx, result) {
                    $('#pgroup-pop').popup('close');
                    form_address.val('');
                    form_phone.val('');
                    form_name.val('');
                    $("#group-district").val('');
                    form_sub_county.val('');
                    form_village.val('');
                   // form_district.val(''); 
                    $( ":mobile-pagecontainer" ).pagecontainer( "change", "#pgroups", { transition: 'flip'} );
                    // alert('Inserted');
               
                },
                function(error){
                     $('#pgroup-pop').popup('close');
                    errors.text('Producer group couldn\'t be added' )
                        .show();
                });
        });

}  
//method to validate a producer
function ProducerValidate (first_name , other_name ,producer_phone_number , producer_address , producer_district , producer_sub_county , producer_village , producer_bank , account_name  , producer_bank_account , errors)
{

   var errors = $('#add-producer-errors');
   errors.attr('style','font-weight: none; color: #FFF; background: #f00; padding: 3px;')
       .hide();
   if(first_name.val() == '' || first_name.val() == null)
   {
       other_name.attr('style', 'border: none;');
       producer_phone_number.attr('style', 'border: none;');
       producer_address.attr('style', 'border: none;');
       producer_district.attr('style', 'border: none;');
       producer_sub_county.attr('style', 'border: none;');
       producer_village.attr('style', 'border: none;');
       producer_bank.attr('style', 'border: none;');
       account_name.attr('style', 'border: none;');
       producer_bank_account.attr('style', 'border: none;');
       errors.text('First name is missing!')
           .show();
       first_name.attr('style', 'border: thin solid #f00;');
       return false;
   }else if(other_name.val() =='' || other_name.val() == null  )
   {
      first_name.attr('style', 'border: none;');
      producer_phone_number.attr('style', 'border: none;');
      producer_address.attr('style', 'border: none;');
      producer_district.attr('style', 'border: none;');
      producer_sub_county.attr('style', 'border: none;');
      producer_village.attr('style', 'border: none;');
      producer_bank.attr('style', 'border: none;');
      account_name.attr('style', 'border: none;');
      producer_bank_account.attr('style', 'border: none;');
      errors.text('Other names is missing!')
             .show();
      other_name.attr('style', 'border: thin solid #f00;');
         return false;
   }else if(producer_phone_number.val() == '' || producer_phone_number.val() == null)
   {
         other_name.attr('style', 'border: none;');
         first_name.attr('style', 'border: none;');
         producer_address.attr('style', 'border: none;');
         producer_district.attr('style', 'border: none;');
         producer_sub_county.attr('style', 'border: none;');
         producer_village.attr('style', 'border: none;');
         producer_bank.attr('style', 'border: none;');
         account_name.attr('style', 'border: none;');
         producer_bank_account.attr('style', 'border: none;');
         errors.text('Phone number is missing!')
            .show();
         producer_phone_number.attr('style', 'border: thin solid #f00;');
        return false;
   }else if(producer_address.val() == '' || producer_address.val() == null)
   {
           other_name.attr('style', 'border: none;');
           first_name.attr('style', 'border: none;');
           producer_phone_number.attr('style', 'border: none;');
           producer_district.attr('style', 'border: none;');
           producer_sub_county.attr('style', 'border: none;');
           producer_village.attr('style', 'border: none;');
           producer_bank.attr('style', 'border: none;');
           account_name.attr('style', 'border: none;');
           producer_bank_account.attr('style', 'border: none;');
           errors.text('Address is missing!')
               .show();
          producer_address.attr('style', 'border: thin solid ##f00;');
          return false;
   }else if(producer_district.val() == '' || producer_district.val() == null)
   {
           other_name.attr('style', 'border: none;');
           first_name.attr('style', 'border: none;');
           producer_phone_number.attr('style', 'border: none;');
           producer_address.attr('style', 'border: none;');
           producer_sub_county.attr('style', 'border: none;');
           producer_village.attr('style', 'border: none;');
           producer_bank.attr('style', 'border: none;');
           account_name.attr('style', 'border: none;');
           producer_bank_account.attr('style', 'border: none;');
          errors.text('District is missing!')
              .show();
          producer_district.attr('style', 'border: thin solid #f00;');
          return false;
   }else if(producer_sub_county.val() == '' || producer_sub_county.val() == null)
   {
             other_name.attr('style', 'border: none;');
              first_name.attr('style', 'border: none;');
              producer_phone_number.attr('style', 'border: none;');
              producer_address.attr('style', 'border: none;');
              producer_district.attr('style', 'border: none;');
              producer_village.attr('style', 'border: none;');
              producer_bank.attr('style', 'border: none;');
              account_name.attr('style', 'border: none;');
              producer_bank_account.attr('style', 'border: none;');
             errors.text('Sub county is missing!')
                 .show();
             producer_sub_county .attr('style', 'border: thin solid #f00;');
             return false;
   }else if(producer_village.val() == '' || producer_village.val() == null)
   {
              other_name.attr('style', 'border: none;');
              first_name.attr('style', 'border: none;');
              producer_phone_number.attr('style', 'border: none;');
              producer_address.attr('style', 'border: none;');
              producer_district.attr('style', 'border: none;');
              producer_sub_county.attr('style', 'border: none;');
              producer_bank.attr('style', 'border: none;');
              account_name.attr('style', 'border: none;');
              producer_bank_account.attr('style', 'border: none;');
             errors.text('Village is missing!')
                 .show();
             producer_village.attr('style', 'border: thin solid #f00;');
             return false;
   }else if(producer_bank.val() == '' || producer_bank.val() == null)
   {
              other_name.attr('style', 'border: none;');
              first_name.attr('style', 'border: none;');
              producer_phone_number.attr('style', 'border: none;');
              producer_address.attr('style', 'border: none;');
              producer_district.attr('style', 'border: none;');
              producer_sub_county.attr('style', 'border: none;');
              producer_village.attr('style', 'border: none;');
              account_name.attr('style', 'border: none;');
              producer_bank_account.attr('style', 'border: none;');
             errors.text('Bank account is missing!')
                 .show();
             producer_bank.attr('style', 'border: thin solid #f00;');
             return false;
   }else if(account_name.val() == '' || account_name.val() == null)
   {
              other_name.attr('style', 'border: none;');
              first_name.attr('style', 'border: none;');
              producer_phone_number.attr('style', 'border: none;');
              producer_address.attr('style', 'border: none;');
              producer_district.attr('style', 'border: none;');
              producer_sub_county.attr('style', 'border: none;');
              producer_village.attr('style', 'border: none;');
              producer_bank.attr('style', 'border: none;');
              producer_bank_account.attr('style', 'border: none;');
              errors.text('Account name is missing!')
                 .show();
             account_name.attr('style', 'border: thin solid #f00;');
             return false;
   }else if(producer_bank_account.val() == '' || producer_bank_account.val() == null)
   {
              other_name.attr('style', 'border: none;');
              first_name.attr('style', 'border: none;');
              producer_phone_number.attr('style', 'border: none;');
              producer_address.attr('style', 'border: none;');
              producer_district.attr('style', 'border: none;');
              producer_sub_county.attr('style', 'border: none;');
              producer_village.attr('style', 'border: none;');
              producer_bank.attr('style', 'border: none;');
              account_name.attr('style', 'border: none;');
              errors.text('Account number is missing!')
                 .show();
             producer_bank_account.attr('style', 'border: thin solid #f00;');
             return false;
   }else
   {
      errors.text('')
           .hide();

      first_name.attr('style', 'border: none;');
      other_name.attr('style', 'border: none;');
      producer_phone_number.attr('style', 'border: none;');
      producer_address.attr('style', 'border: none;');
      producer_bank.attr('style', 'border: none;');
      producer_bank_account.attr('style', 'border: none;');
      producer_village.attr('style', 'border: none;');
      producer_district.attr('style', 'border: none;');
      producer_sub_county.attr('style', 'border: none;');
      producer_bank.attr('style', 'border: none;');
      account_name.attr('style', 'border: none;');
      producer_bank_account.attr('style', 'border: none;');

       return true;

       }
}

//method to add a producer
function AddProducer (first_name, other_names, gender, producer_phone_number, producer_address, producer_district, producer_sub_county, producer_village ,producer_bank ,account_name,producer_bank_account,belong_to_group,individual,choose_producer_group,position_held )
{
    
    
  myDB.transaction(function(transaction) {
        var executeQuery ="INSERT INTO PRODUCERS ( first_name , other_names ,gender , producer_phone ,  producer_address , producer_district , producer_sub_county  ,producer_village , producer_bank , account_name , producer_bank_account  , belong_to_group, individual_producer  ,choose_producer_group, position_held  ) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";             
        transaction.executeSql(executeQuery, [first_name.val(),other_names.val(),gender,producer_phone_number.val(),producer_address.val(),producer_district.val(), producer_sub_county.val(),producer_village.val() ,producer_bank.val() ,account_name.val(), producer_bank_account.val(), belong_to_group.val(),individual.val(),choose_producer_group.val(),position_held.val() ]
            , function(tx, result) {
            
            first_name.val('');other_names.val('');producer_phone_number.val('');producer_address.val('');producer_district.val('');producer_sub_county.val('');producer_village.val('');producer_bank.val('');account_name.val('');producer_bank_account.val(''); belong_to_group.val('');individual.val('');choose_producer_group.val('');position_held.val('');
                $( ":mobile-pagecontainer" ).pagecontainer( "change", "#initial-plan-vivo", { transition: 'flip'} );
            
              //   alert('Inserted');
            },
            function(error){
                 alert('Error occurred'); 
            });
    });   
}

function getPosition() {

   var options = {
      enableHighAccuracy: true,
      maximumAge: 3600000
   }
	
   var watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, options);

   function onSuccess(position) {

       $('#first_latitude').val(position.coords.latitude);
       $('#first_longitude').val(position.coords.longitude);
  /* alert('Latitude: '           + position.coords.latitude          + '\n' +
         'Longitude: '         + position.coords.longitude         + '\n' +
         'Altitude: '          + position.coords.altitude          + '\n' +
         'Accuracy: '          + position.coords.accuracy          + '\n' +
         'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
         'Heading: '           + position.coords.heading           + '\n' +
         'Speed: '             + position.coords.speed             + '\n' +
         'Timestamp: '         + position.timestamp                + '\n');
    */
   };
   function onError(error) {
      alert('code: '    + error.code    + '\n' + 'message: ' + error.message + '\n');
   }
}

function watchPosition() {

   var options = {
      maximumAge: 3600000,
      timeout: 3000,
      enableHighAccuracy: true,
   }

   var watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, options);

   function onSuccess(position) {
       $('#second_latitude').val(position.coords.latitude);
       $('#second_longitude').val(position.coords.longitude);

 /* alert('Latitude: '           + position.coords.latitude          + '\n' +
         'Longitude: '         + position.coords.longitude         + '\n' +
         'Altitude: '          + position.coords.altitude          + '\n' +
         'Accuracy: '          + position.coords.accuracy          + '\n' +
         'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
         'Heading: '           + position.coords.heading           + '\n' +
         'Speed: '             + position.coords.speed             + '\n' +
         'Timestamp: '         + position.timestamp                + '\n');
 */ 
   };

   function onError(error) {
      alert('code: '    + error.code    + '\n' +'message: ' + error.message + '\n');
   }

}

function cordovaDevice() {
    alert(JSON.stringify( device ) );
   /*alert("Cordova version: " + device.cordova + "\n" +
      "Device model: " + device.model + "\n" +
      "Device platform: " + device.platform + "\n" +
      "Device UUID: " + device.uuid + "\n" +
      "Device version: " + device.version);*/
}

