$(document).ready(function(){

var myDB;
var login_url = '';
//Open Database Connection
document.addEventListener("deviceready",onDeviceReady,false);

function onDeviceReady()
{
    myDB = window.sqlitePlugin.openDatabase({name: "mySQLite.db", location: 'default'});
    myDB.transaction(function(tx) {
        //create table
        tx.executeSql("CREATE TABLE IF NOT EXISTS SESSION ( user_id unique ,email text , username text, token text)");
         alert("created sessions table ")
    }, function(err){
        //errors for all transactions are reported here
        alert("Error: " + err.message)
    });
}


//Insert New Data
$("#account-login").click(function(){
     //alert("login: ")
  var email=$("#login-email").val();
  var password=$("#password").val();
  var error = $('#errors').attr('style','font-weight: none; color: #FFF; background: #61a93e;; padding: 3px;')
              .hide();
   
    if(email == '' || email == null)
      {
          alert('Email is required');
          return false;
      }else if(password == '' || password == null)
      {
          alert('Password is required');
          return false;
      }else if(password.length < 6)
      {
              alert('Password should be at least 6 characters');
              return false;
      }else
      { 
           $( ":mobile-pagecontainer" ).pagecontainer( "change", "#home", { transition: 'flip'} );
         // alert(login_url);
          $.post(login_url, {email:email, password:password}, function(data)
          {
             
                var userId = data[0].id,
                    username = data[0].username,
                    email= data[0].email,
                    token = data[0].token;
              
              if(data[0].email = email)
              {
                  $('#loging').popup('close');
                  email.val('');
                  password.val('');
                  errors.text('')
                      .hide();
                   myDB.transaction(function(transaction) {
                    var executeQuery = "INSERT INTO SESSION ( user_id  ,email  , username , token ) VALUES (?,?,?,?)";             
                    transaction.executeSql(executeQuery, [userId , email , username ,token]
                    , function(tx, result) {
                        $( ":mobile-pagecontainer" ).pagecontainer( "change", "#home", { transition: 'flip'} );
                      //  session.switchPage();
                    },
                    function(error){
                         alert('Error occurred'); 
                    });
                });
                  
              }else
              {
                  $('#loging').popup('close');
                  errors.text(data.message)
                      .show();
              }
          },'json');
        
           
      }
  
});

//Display Table Data
$("#showTable").click(function(){
  $("#TableData").html("");
  myDB.transaction(function(transaction) {
  transaction.executeSql('SELECT * FROM phonegap_pro', [], function (tx, results) {
       var len = results.rows.length, i;
       $("#rowCount").html(len);
       for (i = 0; i < len; i++){
          $("#TableData").append("<tr><td>"+results.rows.item(i).id+"</td><td>"+results.rows.item(i).title+"</td><td>"+results.rows.item(i).desc+"</td><td><a href='edit.html?id="+results.rows.item(i).id+"&title="+results.rows.item(i).title+"&desc="+results.rows.item(i).desc+"'>Edit</a> &nbsp;&nbsp; <a class='delete' href='#' id='"+results.rows.item(i).id+"'>Delete</a></td></tr>");
       }
    }, null);
  });
});

//Delete Data from Database
$(document.body).on('click', '.delete' ,function(){
  var id=this.id;
  myDB.transaction(function(transaction) {
    var executeQuery = "DELETE FROM phonegap_pro where id=?";
    transaction.executeSql(executeQuery, [id],
      //On Success
      function(tx, result) {alert('Delete successfully');},
      //On Error
      function(error){alert('Something went Wrong');});
  });
});


//Delete Tables
$("#update").click(function(){
  var id=$("#id").text();
  var title=$("#title").val();
  var desc=$("#desc").val()
  myDB.transaction(function(transaction) {
    var executeQuery = "UPDATE phonegap_pro SET title=?, desc=? WHERE id=?";
    transaction.executeSql(executeQuery, [title,desc,id],
      //On Success
      function(tx, result) {alert('Updated successfully');},
      //On Error
      function(error){alert('Something went Wrong');});
  });
});

$("#DropTable").click(function(){
    myDB.transaction(function(transaction) {
        var executeQuery = "DROP TABLE  IF EXISTS phonegap_pro";
        transaction.executeSql(executeQuery, [],
            function(tx, result) {alert('Table deleted successfully.');},
            function(error){alert('Error occurred while droping the table.');}
        );
    });
});
    
    /* function switchPage(){
         alert(email + ' '+ password);
               }*/
   
    
});


